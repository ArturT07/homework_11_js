document.addEventListener('DOMContentLoaded', function () {
    const passwordInputs = document.querySelectorAll('input[type="password"]');
    const iconPasswords = document.querySelectorAll('.icon-password');
    const submitButton = document.querySelector('.btn');

    iconPasswords.forEach((icon, index) => {
    icon.addEventListener('click', () => {
    if (passwordInputs[index].type === 'password') {
    passwordInputs[index].type = 'text';
    icon.classList.remove('fa-eye');
    icon.classList.add('fa-eye-slash');
} else {
    passwordInputs[index].type = 'password';
    icon.classList.remove('fa-eye-slash');
    icon.classList.add('fa-eye');
}
});
});

    submitButton.addEventListener('click', function (event) {
    event.preventDefault();

    const firstPassword = passwordInputs[0].value;
    const secondPassword = passwordInputs[1].value;

    if (firstPassword === secondPassword) {
    alert('You are welcome');
} else {
    const errorText = document.createElement('p');
    errorText.textContent = 'Потрібно ввести однакові значення';
    errorText.style.color = 'red';
    passwordInputs[1].insertAdjacentElement('afterend', errorText);
}
});
});
